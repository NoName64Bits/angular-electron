import { Component, OnInit } from '@angular/core';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  stagger,
  query
} from '@angular/animations';

import * as moment from 'moment';
import { TranslateModule } from '@ngx-translate/core';
import { ApiService } from '../../providers/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('flyIn', [
      transition('* => *', [
        query(":enter", [          
          style({ opacity: 0 }),
          stagger('100ms', [
            style({
              opacity: 0,
              transform: 'translateX(-100%)'
            }),
            animate('0.2s ease-in')
          ])
        ], {optional: true})
      ]),
      transition('* => void', [
        query(":leave", stagger('100ms', [
          animate('0.2s 0.1s ease-out', style({
            opacity: 0,
            transform: 'translateX(100%)'
          }))
        ]))
      ])
    ]),
    trigger('flyInOut', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: 'translateX(-100%)'
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s 0.1s ease-out', style({
          opacity: 0,
          transform: 'translateX(100%)'
        }))
      ])
    ])
  ]
})

export class HomeComponent implements OnInit {

  constructor(private api: ApiService) {
    let token = localStorage.getItem("atk");
    ApiService.AUTH_TOKEN = token;
  }

  stats = {
    done: 0,
    pending: 0,
    total: 0
  };

  today;
  tasks: Array<Object> = [ ]

  async ngOnInit() {
    this.today = moment().format("dddd, Do of MMMM YYYY");
    try {
      let result: any = await this.api.query("me", `
        query {
          me {
            statistics { ...StatisticsEssentials },
            tasks { ...TasksEssentials }
          }
        }
      `, {}, ["TasksEssentials", "StatisticsEssentials"]);
      console.log(result);
      this.stats = result.data.me.statistics;
      this.tasks = result.data.me.tasks;
    } catch(ex) {
      console.error(ex);
    }
  }

  async addNewTask() {
    // this.tasks.push({
    //   id: 1 + this.tasks.length + "",
    //   name: "New task!",
    //   done: false
    // })
    // this.stats.total++;        
    
    try {
      let result: any = await this.api.query("me", `
        mutation {
          task: newTask { ...TasksEssentials }
        }
      `, {}, ["TasksEssentials"]);
      this.stats.total++;
      this.tasks.push(result.data.task);
      if(result.data.task.done) this.stats.done++;
      else this.stats.pending++;
    } catch (ex) {
      console.error(ex);
    }
  }

  async deleteTask(index) {
    //TODO: NEEDS MORE TESTING
    
    let { id, done } = <any>this.tasks[index];

    try {
      await this.api.query("me", `
        mutation remove($id: String!) {
          removeTask(id: $id)
        }
      `, { id });

      if(done) this.stats.done--;
      else this.stats.pending--;
      
      this.stats.total--;
      this.tasks.splice(index, 1);
    } catch (ex) {
      console.error(ex);
    }
  }

  async taskUpdate(index: string) {
    let {done, id} = this.tasks[index];
    done = !done; //fuck me
    
    try {
      await this.api.query("me", `
        mutation update($id: String!, $done: Boolean){
          updateTask(
            id: $id,
            done: $done
          ) { ...TasksEssentials }
        }
      `, { id, done }, ["TasksEssentials"]);

      if(done) {
        this.stats.pending--;
        this.stats.done++;
      } else {
        this.stats.pending++;
        this.stats.done--;
      }
    } catch(ex) {
      console.error(ex);
    }
  }

  async taskOnBlur(id, name) {
    console.log(id);
    console.log(name);
    try {
      await this.api.query("me", `
        mutation update($id: String!, $name: String){
          updateTask(
            id: $id,
            name: $name
          ) { ...TasksEssentials }
        }
      `, { id, name }, ["TasksEssentials"]);
    } catch(ex) {
      console.error(ex);
    }
  }

}
