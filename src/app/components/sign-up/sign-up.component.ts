import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../providers/api.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  constructor(private router: Router, private api: ApiService) { }

  ngOnInit() { }

  user = {
    email: '',
    password: '',
    confirm: ''
  }

  async signUp() {
    if(this.user.password != this.user.confirm) return;
    try {
      await this.api.register(this.user.email, this.user.password);
      this.router.navigate(['/log-in']);
    } catch(ex) {
      console.error(ex);
    }
  }

}
