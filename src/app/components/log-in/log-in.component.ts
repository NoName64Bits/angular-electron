import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../providers/api.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  constructor(private router: Router, private api: ApiService) { }

  async ngOnInit() {
    let storedToken = localStorage.getItem("atk");
    if(!storedToken)
      return;
    
    ApiService.AUTH_TOKEN = storedToken;
    try {
      let result = await this.api.connect();
      this.router.navigate(['/to-do']);
    } catch(ex){
      console.error(ex);
      return;
    }
  }

  user = {
    email: '',
    password: ''
  }

  async logIn() {
    try {
      await this.api.login(this.user.email, this.user.password);
      localStorage.setItem("atk", ApiService.AUTH_TOKEN);
      this.router.navigate(['/to-do']);      
    } catch(ex) {
      console.error(ex);
    }
  }

}
