import { Injectable } from '@angular/core';

@Injectable()
export class ApiService {
  public static ENDPOINT = "http://localhost:4100/api/v1/";
  public static AUTH_TOKEN = "";
  public static REFRESH_TOKEN = "";

  public static FRAGMENTS: Object = {};


  public static registerFragment(name: string, type: string, fields: string[]) {
    let f = fields.join(", ");
    this.FRAGMENTS[name] = `fragment ${name} on ${type} { ${f} }`;
  }

  public static registerFragments(fragments: {[key: string]: string[]}) {
    let keys = Object.keys(fragments);
    for(let key of keys) {
      let components = key.split(".");
      let name = components[1];
      let type = components[0];

      this.registerFragment(name, type, fragments[key]);
    }
  }

  public static filterFragments(filter: string[]): string[] {
    let keys = Object.keys(this.FRAGMENTS);
    let fragments: string[] = keys.filter(value => filter.includes(value));
    let literals: string[] = [];
    for(let fragment of fragments) {
      literals.push(this.FRAGMENTS[fragment]);
    }

    return literals;
  }

  constructor() { }

  private async post(url: string, data: Object, headers: any = {}): Promise<Object> {
    headers['user-agent'] = "Concreto/4 Mink Client";
    headers['content-type'] = "application/json";

    let response = await fetch(url, {
      body: JSON.stringify(data),
      cache: 'no-cache',
      credentials: 'same-origin',
      headers,
      method: 'POST',
      mode: 'cors',
      redirect: 'follow',
      referrer: 'no-referrer'
    });
    if(!response.ok) {
      throw await response.json();
    }
    return await response.json();
  }

  private async get(url: string, headers: any = {}): Promise<Object> {
    headers['user-agent'] = "Concreto/4 Mink Client";
    headers['content-type'] = "application/json";

    let response = await fetch(url, {
      cache: 'no-cache',
      credentials: 'same-origin',
      headers,
      method: 'GET',
      mode: 'cors',
      redirect: 'follow',
      referrer: 'no-referrer'
    });
    if(!response.ok) {
      throw await response.json();
    }
    return await response.json();
  }

  public async call(method: string, data: Object = null, headers: Object = {}): Promise<Object> {
    let response: any;

    if(typeof data == "undefined" || data == null) {
      response = await this.get(ApiService.ENDPOINT + method, headers);       
    } else {
      response = await this.post(ApiService.ENDPOINT + method, data, headers); 
    }
    
    console.log("server took", response.meta.time.spent,"ms to process");
    return response;
  }

  public async login(email: string, password: string): Promise<boolean> {
    password = btoa(password);
    let response: any = await this.call("auth/login", {email, password});
    
    ApiService.AUTH_TOKEN = response.data.token;
    ApiService.REFRESH_TOKEN = response.data.refresh;

    return true;
  }

  public async register(email: string, password: string): Promise<boolean> {
    password = btoa(password);
    let response: any = await this.call("auth/register", {email, password});
    
    ApiService.AUTH_TOKEN = response.data.token;
    ApiService.REFRESH_TOKEN = response.data.refresh;

    return true;
  }

  public async refresh(): Promise<boolean> {
    let response: any = await this.get("auth/refresh", { "x-refresh-token": ApiService.REFRESH_TOKEN });
    
    ApiService.AUTH_TOKEN = response.data.token;
    ApiService.REFRESH_TOKEN = response.data.refresh;

    return true;
  }
  
  public async authedCall(method: string, data: Object = null) {
    return await this.call(method, data, { "x-access-token": ApiService.AUTH_TOKEN });
  }

  public async connect() {
    return await this.authedCall("auth");
  }

  public async query(graph: string, q: string, variables: Object = {}, fragments: string[] = []): Promise<Object> {
    q = ApiService.filterFragments(fragments).join("\n") + q;
    console.log(q);
    return await this.authedCall(graph + "/query", { query: q, variables })
  }
}
